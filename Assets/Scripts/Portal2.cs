using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(MeshRenderer))]
    public class Portal2 : MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private Portal2 _portal2FromLevel;
        [SerializeField] private ExitPortal2 _exitPortal2;

        private MeshRenderer _renderer;

        private void Awake()
        {
            _renderer = GetComponent<MeshRenderer>();
        }

        public void Update()
        {
            var flatPortal2Position = new Vector2(_portal2FromLevel.transform.position.x, _portal2FromLevel.transform.position.z);
            var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

            if (flatPortal2Position == flatPlayerPosition)
                Teleport();
        }

        public void Teleport()
        {
            _player.transform.position = new Vector3(_exitPortal2.transform.position.x, 0.5f, _exitPortal2.transform.position.z);
        }
    }
}