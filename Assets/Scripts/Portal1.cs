using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(MeshRenderer))]
    public class Portal1: MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private Portal1 _portal1FromLevel;
        [SerializeField] private ExitPortal1 _exitPortal1;

        private MeshRenderer _renderer;

        private void Awake()
        {
            _renderer = GetComponent<MeshRenderer>();
        }

        public void Update()
        {
            var flatPortal1Position = new Vector2(_portal1FromLevel.transform.position.x, _portal1FromLevel.transform.position.z);
            var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

            if (flatPortal1Position == flatPlayerPosition)
                Teleport();
        }

        public void Teleport()
        {
            _player.transform.position = new Vector3(_exitPortal1.transform.position.x, 0.5f, _exitPortal1.transform.position.z);
        }
    }
}