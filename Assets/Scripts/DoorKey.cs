using UnityEngine;

namespace Game
{
    public class DoorKey : MonoBehaviour
    {
        [SerializeField] private Door _door;
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Player player))
            {
                _door.transform.position = new Vector3(0f, 0.5f, 2.5f);
                Destroy(gameObject);
            }
        }
    }
}